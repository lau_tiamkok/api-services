<!DOCTYPE html>
<html>
    <style>
    html, body, #map-canvas {
        height: 100%;
        width: 100%;
        margin: 0px;
        padding: 0px
    }
  </style>
        
    <script src="jquery-min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>
        
        // Set vars.
        var map;
        var marker;
        var myCenter = new google.maps.LatLng(-36.8484597,174.7633315); // Auckland's.

        function initialize()
        {
            var mapProp = {
                center:myCenter,
                zoom:3,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById("map-canvas"), mapProp);

            marker = new google.maps.Marker({
                position:myCenter
            });
            
            // Set the default marker on load.
            marker.setMap(map);
            
            // This event listener will call placeMarker() when the map is clicked.
            google.maps.event.addListener(map, 'click', function(event) {
                
                // Remove the prev marker.
                if(marker){
                    marker.setMap(null);
                }
                
                // Debug.
                console.log(event.latLng);
                
                // Get lat and lng.
                var latLng = event.latLng;
                var lat = latLng.lat();
                var lng = latLng.lng();
                
                // Debug.
                console.log("lat: " + lat);
                console.log("lng: " + lng);
                
                // Set the answer.
                $("#latLng").text(lat + " / " + lng);
                
                // Place the new marker.
                placeMarker(event.latLng);
            
            });
        }
        
        function placeMarker(location) {
            
            marker = new google.maps.Marker({
                position: location, 
                map: map
            });
            
            $.ajax({
                
               url:"https://maps.googleapis.com/maps/api/timezone/json?location="+location.k+","+location.D+"&timestamp="+(Math.round((new Date().getTime())/1000)).toString()+"&sensor=false",
               
            }).done(function(response){
                
               if(response.timeZoneId != null){
                   
                    // Update the texts.
                    $("#timezone").text(response.timeZoneId);
                    
                    // Set local and utc times.
                    setTimes(response.rawOffset);
                    
               }
               
               // Debug.
               console.log(response);
               
            });

        }
        
        function setTimes(offset) {
            
            var d = new Date();
            var utc = d.getTime() + (d.getTimezoneOffset() * 60000);
            var nd = new Date(utc + (1000*offset));
            
            // Debug.
            console.log("utc timestamp: " + utc);
            console.log("utc timestamp to human readable time: " + new Date(utc).toLocaleString());
                    
            // Calculate the time.
            var local = nd.toLocaleString();
            var utc = new Date(utc).toLocaleString();
            
            $("#utc-time").text(utc);
            $("#local-time").text(local);
        }

        google.maps.event.addDomListener(window, 'load', initialize);
       
      
    </script>

</head>
<body>
    
    <p>Latitude / longitude: <span id="latLng"></span></p>
    <p>Timezone based on location: <span id="timezone"></span></p>
    <p>Current UTC time : <span id="utc-time"></span></p>
    <p>Current local time : <span id="local-time"></span></p>
    <div id="map-canvas"></div>
    
</body>
</html>